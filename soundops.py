import math
from mysound import Sound


def soundadd(s1: Sound, s2: Sound) -> Sound:

    #  Buffers de los sonidos
    sonido1 = s1.buffer
    sonido2 = s2.buffer

    # comparar tamanyos de los buffers
    if len(sonido2) > len(sonido1):
        sonido1 = sonido1 + [0] * (len(sonido2) - len(sonido1))
    else:
        sonido2 = sonido2 + [0] * (len(sonido1) - len(sonido2))

    # Sumar muestras de los buffers
    sonido3 = [x + y for x, y in zip(sonido1, sonido2)]
    return sonido3
