import unittest

from soundops import soundadd
from soundops import Sound

class TestSoundAdd(unittest.TestCase):

        def test_lenght(self):
            s1 = Sound(1)  # objeto Sound 1 de 1 segundo
            s1.sin(440, 10000)
            s2 = Sound(2)  # objeto Sound 2 de 2 segundos
            s2.sin(660, 10000)
            s3 = soundadd(s1, s2)

            # Probar longitud de los buffers
            self.assertEqual(44100, len(s1.buffer))
            self.assertEqual(88200, len(s2.buffer))

            mayorBuffer=max(len(s1.buffer),len(s2.buffer))
            self.assertEqual(88200, mayorBuffer)

            # Probar que el buffer resultante tiene la longitud del buffer mas largo original
            self.assertEqual(len(s3), mayorBuffer)


        def test_values(self):
            s1 = Sound(1)
            s1.sin(440, 10000)
            s2 = Sound(2)
            s2.sin(660, 10000)
            s3= soundadd(s1, s2)

            # Probar valores de las muestras
            self.assertEqual(-11789, s3[60])
            self.assertEqual(-18788, s3[2785])
            self.assertEqual(-1605, s3[41600])



        def test_sum(self):
            s1 = Sound(0.5)
            s1.sin(600, 10000)
            s2 = Sound(1)
            s2.sin(440, 5000)
            s3 = soundadd(s1, s2)

            # Probar que los valores del buffer resultante son la suma de los buffers originales
            valorPosicion1 = s1.buffer[345] + s2.buffer[345]
            valorPosicion2 = s1.buffer[8032] + s2.buffer[8032]
            self.assertTrue(valorPosicion1, s3[345])
            self.assertTrue(valorPosicion2, s3[8032])


if __name__ == '__main__':
    unittest.main()


