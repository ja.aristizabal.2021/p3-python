import unittest
from mysound import Sound

class TestMio(unittest.TestCase):

    def test_values(self):
        sound = Sound(1)
        sound.sin(220, 5000)

        # Comprobar que valores determinados tienen un cierto valor
        self.assertEqual(0, sound.buffer[0])
        self.assertEqual(0, sound.buffer[11025])
        self.assertEqual(0, sound.buffer[22050])

        sound = Sound(2)
        sound.sin(1000, 500)

        # Comprueba el que los valores se encuentren en el rango esperado
        self.assertTrue(all(-Sound.max_amplitude <= x <= Sound.max_amplitude
                            for x in sound.buffer))

        # Comprobar que valores determinados tienen un cierto valor
        self.assertEqual(461, sound.buffer[543])
        self.assertEqual(32, sound.buffer[11223])
        self.assertEqual(0, sound.buffer[44100])

if __name__ == '__main__':
    unittest.main()

