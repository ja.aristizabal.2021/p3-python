import unittest
from mysoundsin import SoundSin
from mysoundsin import Sound

class TestSoundSin(unittest.TestCase):

    def test_values(self):
        soundsin = SoundSin(2, 440, 10000)

        # Rango de valores del seno se mueven entre los valores esperados
        self.assertTrue(all(-SoundSin.max_amplitude <= x <= SoundSin.max_amplitude
                            for x in soundsin.buffer))

        # Valor exacto de muestras en determinados puntos
        self.assertEqual(0, soundsin.buffer[0])
        self.assertEqual(0, soundsin.buffer[11025])
        self.assertEqual(0, soundsin.buffer[22050])
    def test_sound_compare(self):
        # Comparar resultados del seno en mysound y en mysoundsin
        sound = Sound(2)
        sound1 = sound.sin(440, 10000)
        sound2 = SoundSin(2,440,10000)
        self.assertTrue(sound1, sound2)

    def test_nsamples(self):

        # longitud del buffer y numero de muestras deben ser iguales
        soundsin = SoundSin(2,440,10000)
        self.assertTrue(44100 * 2, len(soundsin.buffer))
        self.assertTrue(44100 * 2, soundsin.nsamples)


if __name__ == '__main__':
    unittest.main()

