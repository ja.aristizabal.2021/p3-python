import math
from mysound import Sound


class SoundSin(Sound):

    def __init__(self, duration, frequency, amplitude):
        super().__init__(duration)
        self.frequency = frequency
        self.amplitude = amplitude
        self.buffer = [0] * self.nsamples
        self.sin(self.frequency, self.amplitude)
